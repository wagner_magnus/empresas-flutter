import 'package:flutter/material.dart';
import 'package:ioasysapp/app/shared/helpers/fonts_helper.dart';
import 'package:ioasysapp/theme/app_colors.dart';

mixin TextStyles {
  static const double _linkFontSize = 16;
  static const double _titleFontSize = 18;
  static const double _textFontSize = 14;
  // static const double _miniTextFontSize = 12;
  static const double _superTitleFontSize = 20;

  static const TextStyle superTitle = TextStyle(
    fontSize: _superTitleFontSize,
    fontFamily: FontsHelper.regular,
    color: AppColors.white,
  );

  static const TextStyle title = TextStyle(
    fontSize: _linkFontSize,
    fontFamily: FontsHelper.regular,
    color: AppColors.white,
  );
  static const TextStyle mono1 = TextStyle(
    fontSize: _titleFontSize,
    fontFamily: FontsHelper.bold,
    color: AppColors.white,
  );
  static TextStyle mono2 = TextStyle(
    fontSize: _textFontSize,
    fontFamily: FontsHelper.light,
    color: AppColors.hintColor,
  );
}
