import 'dart:ui';

class AppColors {
  static const pink = Color(0xFFE01E69);
  static const gray = Color(0xFFF5F5F5);
  static const white = Color(0xFFFFFFFF);
  static const mono = Color(0xff606C80);
  static const red = Color(0xFFE00000);
  static const enterpriseColor = Color(0xFF79BBCA);
  static var hintColor = const Color(0xFF666666).withOpacity(.7);
}
