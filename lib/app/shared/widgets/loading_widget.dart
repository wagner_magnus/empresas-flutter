import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';

class LoadingWidget extends StatefulWidget {
  @override
  _LoadingStateWidget createState() => _LoadingStateWidget();
}

class _LoadingStateWidget extends State<LoadingWidget> with TickerProviderStateMixin {
  AnimationController _controller1;
  AnimationController _controller2;

  @override
  void initState() {
    super.initState();

    _controller1 = AnimationController(vsync: this, duration: const Duration(seconds: 1))..repeat();
    _controller2 = AnimationController(vsync: this, duration: const Duration(seconds: 5))..repeat();
  }

  @override
  void dispose() {
    _controller1.dispose();
    _controller2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Container(
        child: Stack(
          alignment: Alignment.center,
          children: [
            AnimatedBuilder(
              animation: _controller1,
              builder: (_, child) => Transform.rotate(
                angle: _controller1.value * 5 * math.pi,
                child: child,
              ),
              child: SvgPicture.asset(AssetsPathHelper.ellipse3),
            ),
            AnimatedBuilder(
              animation: _controller2,
              builder: (_, child) => Transform.rotate(
                angle: _controller2.value * 20,
                child: child,
              ),
              child: SvgPicture.asset(AssetsPathHelper.ellipse2),
            ),
          ],
        ),
      );
}
