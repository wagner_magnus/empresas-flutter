import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class BaseDio {
  final Dio dio;
  final BaseOptions options = BaseOptions(
    baseUrl: DotEnv().env['BASE_URL'],
  );

  BaseDio(this.dio);

  Dio createDio() {
    dio.options = options;
    return dio;
  }
}
