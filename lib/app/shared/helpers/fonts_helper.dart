class FontsHelper {
  static const String light = 'RubikLight';
  static const String regular = 'RubikRegular';
  static const String medium = 'RubikMedium';
  static const String bold = 'RubikBold';
}