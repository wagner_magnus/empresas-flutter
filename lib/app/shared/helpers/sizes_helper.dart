import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SizesHelper {
  static double getHeight() => MediaQuery.of(Modular.navigatorKey.currentContext).size.height;

  static double getWidth() => MediaQuery.of(Modular.navigatorKey.currentContext).size.width;

  static double getStatusBarHeight() => MediaQuery.of(Modular.navigatorKey.currentContext).padding.top;

  static double getBottomSheetHeight() => MediaQuery.of(Modular.navigatorKey.currentContext).padding.bottom;

  static double getToolBarHeight() => kToolbarHeight;

  static double getBottomNavigationHeight() => kBottomNavigationBarHeight;

  static double getWidthByPercentage(int percentage) {
    if (percentage > 100 || 0 > percentage) {
      throw Exception('A porcentagem deve ser maior que 0 e menor ou igual a 100.');
    }
    return getWidth() * (percentage / 100);
  }

  static double getHeightByPercentage(int percentage) {
    if (percentage > 100 || 0 > percentage) {
      throw Exception('A porcentagem deve ser maior que 0 e menor ou igual a 100.');
    }
    return getHeight() * (percentage / 100);
  }
}
