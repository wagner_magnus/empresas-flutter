class AssetsPathHelper {
  static const background = 'assets/images/background.png';
  static const logoHome = 'assets/images/logo_home.png';
  static const logo = 'assets/images/logo.png';
  static const ellipse2 = 'assets/images_svg/ellipse_2.svg';
  static const ellipse3 = 'assets/images_svg/ellipse_3.svg';
  static const error = 'assets/images/error.png';
  static const logo1 = 'assets/images/logo_1.png';
  static const logo2 = 'assets/images/logo_2.png';
  static const logo3 = 'assets/images/logo_3.png';
  static const logo4 = 'assets/images/logo_4.png';
}
