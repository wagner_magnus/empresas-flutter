import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class NavigatorHelper {
  static Future pushNamedTo(String routeName, {Object arguments}) => Modular.to.pushNamed(routeName, arguments: arguments);

  static Future pushNamedLink(String routeName, {Object arguments}) => Modular.link.pushNamed(routeName, arguments: arguments);

  static Future pushNamedAndRemoveUntilToModuleTo(String routeName, {Object arguments}) => Modular.to.pushNamedAndRemoveUntil(
        routeName,
        (Route<dynamic> route) => false,
        arguments: arguments,
      );

  static Future pushNamedAndRemoveUntilLink(String routeName, {Object arguments}) => Modular.link.pushNamedAndRemoveUntil(
        routeName,
        (Route<dynamic> route) => false,
        arguments: arguments,
      );

  static Future pushReplacementNamedTo(String routeName, {Object arguments}) => Modular.to.pushReplacementNamed(routeName, arguments: arguments);

  static Future pushReplacementNamedLink(String routeName, {Object arguments}) => Modular.link.pushReplacementNamed(routeName, arguments: arguments);

  //ignore:avoid_annotating_with_dynamic
  static void pop({dynamic callBackData}) => Modular.to.pop(callBackData);

  static void maybePop() => Modular.to.maybePop();
}
