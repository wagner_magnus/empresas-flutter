import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ioasysapp/app/shared/widgets/loading_widget.dart';

class LoaderHelper {
  final BuildContext _context;

  LoaderHelper.of(this._context);

  void showLoading() => showDialog(
        barrierDismissible: false,
        useSafeArea: true,
        context: _context,
        builder: (_) => WillPopScope(
          onWillPop: () async => false,
          child: LoadingWidget(),
        ),
      );
}
