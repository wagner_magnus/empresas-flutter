import 'package:ioasysapp/app/core/domain/errors/base_errors/datasource_error.dart';
import 'package:ioasysapp/app/core/domain/errors/base_errors/datasource_error_interface.dart';

abstract class IAuthenticationError extends DataSourceError implements IDataSourceError {}

class AuthenticationError implements IAuthenticationError {
  @override
  String message;

  AuthenticationError({this.message});
}

class InvalidAuthenticationError implements IAuthenticationError {
  @override
  String message;

  InvalidAuthenticationError({this.message});
}