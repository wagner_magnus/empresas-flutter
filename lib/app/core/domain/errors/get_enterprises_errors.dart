import 'package:ioasysapp/app/core/domain/errors/base_errors/datasource_error.dart';
import 'package:ioasysapp/app/core/domain/errors/base_errors/datasource_error_interface.dart';

abstract class IGetEnterprisesError extends DataSourceError implements IDataSourceError {}

class GetEnterprisesError implements IGetEnterprisesError {
  @override
  String message;

  GetEnterprisesError({this.message});
}

class InvalidFilterError implements IGetEnterprisesError {
  @override
  String message;

  InvalidFilterError({this.message});
}