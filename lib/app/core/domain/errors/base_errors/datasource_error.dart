
import 'datasource_error_interface.dart';

class DataSourceError implements IDataSourceError {
  @override
  String message;

  DataSourceError({this.message});
}
