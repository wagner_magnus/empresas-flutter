import 'package:dartz/dartz.dart';
import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/authentication_errors.dart';
import 'package:ioasysapp/app/core/domain/repositories/authentication_repository_interface.dart';
import 'package:validadores/ValidarEmail.dart';

import 'authentication_usecase_interface.dart';

class AuthenticationUsecase implements IAuthenticationUsecase {
  final IAuthenticationRepository _repository;

  AuthenticationUsecase(this._repository);

  @override
  Future<Either<IAuthenticationError, OAuthTokenEntity>> authenticate(AuthenticateEntity authenticateEntity) async {
    if (authenticateEntity == null ||
        (authenticateEntity.email == null ||
            authenticateEntity.email.isEmpty ||
            !EmailValidator.validate(authenticateEntity.email) ||
            authenticateEntity.password == null ||
            authenticateEntity.password.isEmpty)) {
      return Left(InvalidAuthenticationError());
    }

    return _repository.authenticate(authenticateEntity);
  }
}
