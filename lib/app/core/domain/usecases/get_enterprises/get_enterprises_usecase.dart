import 'package:dartz/dartz.dart';
import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/get_enterprises_errors.dart';
import 'package:ioasysapp/app/core/domain/repositories/get_enterprises_repository_interface.dart';
import 'package:ioasysapp/app/core/domain/usecases/get_enterprises/get_enterprises_usecase_interface.dart';

class GetEnterprisesUsecase implements IGetEnterprisesUsecase {
  final IGetEnterprisesRepository _repository;

  GetEnterprisesUsecase(this._repository);

  @override
  Future<Either<IGetEnterprisesError, List<EnterpriseEntity>>> getEnterprisesByName(String name) async {
    if (name == null || name.isEmpty) {
      return Left(InvalidFilterError());
    }

    return _repository.getEnterprisesByName(name);
  }
}
