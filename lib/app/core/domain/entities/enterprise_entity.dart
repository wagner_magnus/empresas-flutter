import 'package:json_annotation/json_annotation.dart';

part 'enterprise_entity.g.dart';

@JsonSerializable()
class EnterpriseEntity {
  final int id;
  final String description;
  final String photo;
  @JsonKey(name: 'enterprise_name')
  final String enterpriseName;

  EnterpriseEntity({this.id, this.enterpriseName, this.photo, this.description});

  factory EnterpriseEntity.fromJson(Map<String, dynamic> json) => _$EnterpriseEntityFromJson(json);

  Map<String, dynamic> toJson() => _$EnterpriseEntityToJson(this);
}