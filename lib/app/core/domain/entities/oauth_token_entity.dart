import 'package:json_annotation/json_annotation.dart';

part 'oauth_token_entity.g.dart';

@JsonSerializable(nullable: true)
class OAuthTokenEntity{
  String accessToken;
  String client;
  String uid;

  OAuthTokenEntity({this.accessToken, this.client, this.uid});

  factory OAuthTokenEntity.fromJson(Map<String, dynamic> json) => _$OAuthTokenEntityFromJson(json);

  Map<String, dynamic> toJson() => _$OAuthTokenEntityToJson(this);
}