import 'package:json_annotation/json_annotation.dart';

part 'authenticate_entity.g.dart';

@JsonSerializable(nullable: true)
class AuthenticateEntity {
  String email;
  String password;

  AuthenticateEntity({this.email, this.password});

  factory AuthenticateEntity.fromJson(Map<String, dynamic> json) => _$AuthenticateEntityFromJson(json);

  Map<String, dynamic> toJson() => _$AuthenticateEntityToJson(this);
}
