import 'package:dartz/dartz.dart';
import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/authentication_errors.dart';

abstract class IAuthenticationRepository {
  Future<Either<IAuthenticationError, OAuthTokenEntity>> authenticate(AuthenticateEntity authenticateEntity);
}
