import 'package:dartz/dartz.dart';
import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/get_enterprises_errors.dart';

abstract class IGetEnterprisesRepository {
  Future<Either<IGetEnterprisesError, List<EnterpriseEntity>>> getEnterprisesByName(String name);
}
