import 'package:dio/dio.dart';
import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';
import 'package:ioasysapp/app/core/infra/datasources/authenticate_datasource_interface.dart';

class AuthenticationDatasource implements IAuthenticationDatasource {
  final Dio dio;

  static const String route = '/api/v1/users/auth';

  AuthenticationDatasource(this.dio);

  @override
  Future<OAuthTokenEntity> authenticate(AuthenticateEntity authenticateEntity) async {
    final response = await dio.post('$route/sign_in', data: authenticateEntity.toJson());

    final token = response.headers.map['access-token'].first;
    final client = response.headers.map['client'].first;
    final uid = response.headers.map['uid'].first;

    final result = OAuthTokenEntity(accessToken: token, client: client, uid: uid);
    return result;
  }
}
