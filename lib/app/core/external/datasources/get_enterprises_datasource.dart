import 'package:dio/dio.dart';
import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';
import 'package:ioasysapp/app/core/infra/datasources/get_enterprises_datasource_interface.dart';

class GetEnterprisesDatasource implements IGetEnterprisesDatasource {
  final Dio dio;

  static const String route = '/api/v1/enterprises';

  GetEnterprisesDatasource(this.dio);

  @override
  Future<List<EnterpriseEntity>> getEnterprisesByName(String name) async {
    final response = await dio.post(route, queryParameters: {'name': name});

    return (response.data['enterprises'] as List)?.map((e) => EnterpriseEntity.fromJson(e))?.toList();
  }
}
