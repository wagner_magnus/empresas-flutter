import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/authentication_errors.dart';
import 'package:ioasysapp/app/core/domain/repositories/authentication_repository_interface.dart';
import 'package:ioasysapp/app/core/infra/datasources/authenticate_datasource_interface.dart';
import 'package:ioasysapp/app/core/infra/repositories/base_repository/base_repository.dart';

class AuthenticationRepository extends BaseRepository implements IAuthenticationRepository {
  final IAuthenticationDatasource datasource;

  AuthenticationRepository(this.datasource);

  @override
  Future<Either<IAuthenticationError, OAuthTokenEntity>> authenticate(AuthenticateEntity authenticateEntity) async {
    try {
      final result = await datasource.authenticate(authenticateEntity);
      return Right(result);
    } on DioError catch (e) {
      switch (e.response.statusCode) {
        case HttpStatus.badRequest:
          {
            return Left(AuthenticationError(message: e.response.data['message']));
          }
        default:
          return Left(AuthenticationError(message: defaultMessageError));
      }
    } on Exception catch (_) {
      return Left(AuthenticationError(message: defaultMessageError));
    }
  }
}
