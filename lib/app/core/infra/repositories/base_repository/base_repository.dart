abstract class BaseRepository {
  final defaultMessageNoInternet = 'Ocorreu um erro, verifique sua conexão e tente novamente.';
  final defaultMessageError = 'Ocorreu um erro, tente novamente dentro de alguns minutos.';
}
