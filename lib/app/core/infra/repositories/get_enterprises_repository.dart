import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';
import 'package:ioasysapp/app/core/domain/errors/get_enterprises_errors.dart';
import 'package:ioasysapp/app/core/domain/repositories/get_enterprises_repository_interface.dart';
import 'package:ioasysapp/app/core/infra/datasources/get_enterprises_datasource_interface.dart';
import 'package:ioasysapp/app/core/infra/repositories/base_repository/base_repository.dart';

class GetEnterprisesRepository extends BaseRepository implements IGetEnterprisesRepository {
  final IGetEnterprisesDatasource datasource;

  GetEnterprisesRepository(this.datasource);

  @override
  Future<Either<IGetEnterprisesError, List<EnterpriseEntity>>> getEnterprisesByName(String name) async {
    try {
      final result = await datasource.getEnterprisesByName(name);
      return Right(result);
    } on DioError catch (e) {
      switch (e.response.statusCode) {
        case HttpStatus.badRequest:
          {
            return Left(GetEnterprisesError(message: defaultMessageError));
          }
        default:
          return Left(GetEnterprisesError(message: defaultMessageError));
      }
    } on Exception catch (_) {
      return Left(GetEnterprisesError(message: defaultMessageError));
    }
  }
}
