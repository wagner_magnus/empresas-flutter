import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';

abstract class IAuthenticationDatasource {
  Future<OAuthTokenEntity> authenticate(AuthenticateEntity authenticateEntity);
}
