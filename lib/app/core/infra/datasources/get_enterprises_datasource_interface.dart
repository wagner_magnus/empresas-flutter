import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';

abstract class IGetEnterprisesDatasource {
  Future<List<EnterpriseEntity>> getEnterprisesByName(String name);
}
