import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/modules/splashscreen/presenters/splashscreen/splashscreen_controller.dart';
import 'package:ioasysapp/app/modules/splashscreen/presenters/splashscreen/splashscreen_page.dart';

class SplashscreenModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => SplashscreenController()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => const SplashscreenPage()),
      ];

  static Inject get to => Inject<SplashscreenModule>.of();
}
