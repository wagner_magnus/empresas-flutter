import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';

import 'splashscreen_controller.dart';

class SplashscreenPage extends StatefulWidget {
  const SplashscreenPage();

  @override
  _SplashscreenPageState createState() => _SplashscreenPageState();
}

class _SplashscreenPageState extends ModularState<SplashscreenPage, SplashscreenController> {
  @override
  void initState() {
    super.initState();
    controller.awaitAndPushPage();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
          alignment: Alignment.center,
          width: double.maxFinite,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(AssetsPathHelper.background),
            ),
          ),
          child: Image.asset(AssetsPathHelper.logoHome, scale: 1),
        ),
      );
}
