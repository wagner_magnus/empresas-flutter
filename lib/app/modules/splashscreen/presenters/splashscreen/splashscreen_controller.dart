import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/shared/helpers/app_routes_helper.dart';
import 'package:ioasysapp/app/shared/helpers/navigator_helper.dart';
import 'package:mobx/mobx.dart';

part 'splashscreen_controller.g.dart';

@Injectable()
class SplashscreenController = _SplashscreenControllerBase with _$SplashscreenController;

abstract class _SplashscreenControllerBase with Store {
  Future<void> awaitAndPushPage() async {
    Future.delayed(const Duration(seconds: 2), () async {
      getStartPage();
    });
  }

  Future<void> getStartPage() async => NavigatorHelper.pushNamedAndRemoveUntilToModuleTo(AppRoutesHelper.login);
}
