import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/core/domain/repositories/get_enterprises_repository_interface.dart';
import 'package:ioasysapp/app/core/domain/usecases/get_enterprises/get_enterprises_usecase.dart';
import 'package:ioasysapp/app/core/domain/usecases/get_enterprises/get_enterprises_usecase_interface.dart';
import 'package:ioasysapp/app/core/external/datasources/get_enterprises_datasource.dart';
import 'package:ioasysapp/app/core/infra/datasources/get_enterprises_datasource_interface.dart';
import 'package:ioasysapp/app/core/infra/repositories/get_enterprises_repository.dart';
import 'package:ioasysapp/app/modules/home/presenters/home/home_controller.dart';
import 'package:ioasysapp/app/modules/home/presenters/home/home_page.dart';

import 'presenters/company_detail/company_detail_controller.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController(i.get<IGetEnterprisesUsecase>())),
        Bind((i) => CompanyDetailController()),
        Bind((i) => GetEnterprisesDatasource(i.get<Dio>())),
        Bind((i) => GetEnterprisesRepository(i.get<IGetEnterprisesDatasource>())),
        Bind((i) => GetEnterprisesUsecase(i.get<IGetEnterprisesRepository>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => const HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
