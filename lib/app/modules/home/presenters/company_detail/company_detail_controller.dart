import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'company_detail_controller.g.dart';

@Injectable()
class CompanyDetailController = _CompanyDetailControllerBase
    with _$CompanyDetailController;

abstract class _CompanyDetailControllerBase with Store {
  @observable
  int value = 0;

  @action
  void increment() {
    value++;
  }
}
