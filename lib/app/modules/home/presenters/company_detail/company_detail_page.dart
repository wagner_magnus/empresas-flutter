import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'company_detail_controller.dart';

class CompanyDetailPage extends StatefulWidget {
  const CompanyDetailPage({Key key})
      : super(key: key);

  @override
  _CompanyDetailPageState createState() => _CompanyDetailPageState();
}

class _CompanyDetailPageState
    extends ModularState<CompanyDetailPage, CompanyDetailController> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
        title: const Text('Companys'),
      ),
      body: Column(
        children: const <Widget>[],
      ),
    );
}
