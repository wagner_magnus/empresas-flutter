import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/modules/home/presenters/home/widgets/custom_header_widget.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';
import 'package:ioasysapp/app/shared/helpers/sizes_helper.dart';
import 'package:ioasysapp/app/shared/widgets/loading_widget.dart';
import 'package:ioasysapp/theme/text_styles.dart';

import 'home_controller.dart';
import 'home_states.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Container(
          child: CustomScrollView(
            controller: _scrollController,
            slivers: <Widget>[
              SliverPersistentHeader(
                pinned: true,
                delegate: CustomHeaderWidget(_scrollController, searchEnterprise: controller.searchEnterprises),
              ),
              SliverFixedExtentList(
                itemExtent: 50,
                delegate: SliverChildListDelegate(
                  [
                    Observer(
                      builder: (_) {
                        final state = controller.state;
                        if (state is SuccessHomeState && state.enterprisesList.isNotEmpty) {
                          return _buildEnterprisesFounded(state.enterprisesList.length);
                        }
                        return const Offstage();
                      },
                    ),
                  ],
                ),
              ),
              Observer(builder: (_) {
                final state = controller.state;
                if (state is LoadingHomeState) {
                  return SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(
                        bottom: SizesHelper.getHeightByPercentage(90),
                        top: SizesHelper.getHeightByPercentage(30),
                      ),
                      child: LoadingWidget(),
                    ),
                  );
                }
                if (state is SuccessHomeState) {
                  if (state.enterprisesList.isEmpty) {
                    return Container(
                      height: MediaQuery.of(context).size.height * .6,
                      alignment: Alignment.center,
                      child: const Text('Nenhum resultado encontrado'),
                    );
                  }
                  return SliverPadding(
                    padding: EdgeInsets.only(bottom: SizesHelper.getHeightByPercentage(90)),
                    sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) => _buildListEnterprises(),
                      childCount: state.enterprisesList.length,
                    )),
                  );
                }
                if (state is ErrorHomeState) {
                  return SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.only(bottom: SizesHelper.getHeightByPercentage(90)),
                      alignment: Alignment.center,
                      child: Padding(
                        padding: EdgeInsets.only(top: SizesHelper.getHeightByPercentage(30)),
                        child: const Text('Nenhum resultado encontrado'),
                      ),
                    ),
                  );
                }
                return SliverToBoxAdapter(
                    child: SizedBox(
                  height: SizesHelper.getHeightByPercentage(90),
                ));
              }),
            ],
          ),
        ),
      );

  Widget _buildEnterprisesFounded(int lenghtListEnterprise) => Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Visibility(
          visible: lenghtListEnterprise > 0,
          child: Text('0$lenghtListEnterprise resultados encontrados', style: TextStyles.mono2),
        ),
      );

  Widget _buildListEnterprises() => Container(
        decoration: BoxDecoration(
          image: const DecorationImage(fit: BoxFit.cover, image: AssetImage(AssetsPathHelper.background)),
          borderRadius: BorderRadius.circular(4),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        height: 120,
        child: const Center(
            child: Text(
          'Olá',
          textAlign: TextAlign.center,
          style: TextStyles.mono1,
        )),
      );
}
