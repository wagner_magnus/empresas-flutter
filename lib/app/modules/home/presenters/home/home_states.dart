import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';

abstract class HomeStates {}

class StartHomeState implements HomeStates {
  const StartHomeState();
}

class LoadingHomeState implements HomeStates {
  const LoadingHomeState();
}

class SuccessHomeState implements HomeStates {
  final List<EnterpriseEntity> enterprisesList;

  const SuccessHomeState(this.enterprisesList);
}

class ErrorHomeState implements HomeStates {
  const ErrorHomeState();
}
