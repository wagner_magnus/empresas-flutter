import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ioasysapp/theme/app_colors.dart';

class CustomSearch extends StatelessWidget {
  final Function onTap;
  final Function(String) onChanged;

  const CustomSearch({Key key, this.onTap, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          onTap: onTap,
          onChanged: onChanged,
          decoration: InputDecoration(
              hintText: 'Pesquise por empresa',
              prefixIcon: Icon(Icons.search, color: AppColors.hintColor, size: 30),
              border: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(4)),
              filled: true,
              fillColor: AppColors.gray,
              contentPadding: const EdgeInsets.all(12)),
        ),
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty<Function>('onTap', onTap))..add(DiagnosticsProperty<Function(String p1)>('onChanged', onChanged));
  }
}
