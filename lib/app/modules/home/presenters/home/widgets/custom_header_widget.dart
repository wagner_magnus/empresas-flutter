import 'package:flutter/material.dart';
import 'package:ioasysapp/app/modules/home/presenters/home/widgets/custom_search.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';
import 'package:ioasysapp/app/shared/helpers/sizes_helper.dart';

class CustomHeaderWidget extends SliverPersistentHeaderDelegate {
  ScrollController scrollController;
  Function(String) searchEnterprise;
  CustomHeaderWidget(this.scrollController, {this.searchEnterprise});
  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) => LayoutBuilder(builder: (context, constraints) {
        final percentage = (constraints.maxHeight - minExtent) / (maxExtent - minExtent);

        return Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              height: double.maxFinite,
              width: double.maxFinite,
              margin: const EdgeInsets.only(bottom: 40),
              decoration: const BoxDecoration(image: DecorationImage(fit: BoxFit.cover, image: AssetImage(AssetsPathHelper.background))),
              child: Opacity(
                opacity: percentage,
                child: Stack(
                  children: [
                    Positioned(left: 0, top: 80, child: Image.asset(AssetsPathHelper.logo1, height: 150, fit: BoxFit.contain)),
                    Positioned(left: 40, child: Image.asset(AssetsPathHelper.logo2, height: 180, fit: BoxFit.contain)),
                    Positioned(top: 80, right: 50, child: Image.asset(AssetsPathHelper.logo3, height: 130, fit: BoxFit.contain)),
                    Positioned(right: 0, top: 0, child: Image.asset(AssetsPathHelper.logo4, height: 140, fit: BoxFit.contain)),
                  ],
                ),
              ),
            ),
            CustomSearch(
              onTap: navigateToTop,
              onChanged: (value) {
                navigateToTop();
                searchEnterprise(value);
              },
            ),
          ],
        );
      });

  void navigateToTop() {
    scrollController.animateTo(SizesHelper.getHeightByPercentage(20), duration: const Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  @override
  // ignore: avoid_renaming_method_parameters
  bool shouldRebuild(SliverPersistentHeaderDelegate sliverPersistentHeaderDelegate) => true;

  @override
  double get maxExtent => 250;

  @override
  double get minExtent => 100;
}
