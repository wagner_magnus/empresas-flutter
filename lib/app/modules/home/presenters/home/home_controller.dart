import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/core/domain/entities/enterprise_entity.dart';
import 'package:ioasysapp/app/core/domain/usecases/get_enterprises/get_enterprises_usecase_interface.dart';
import 'package:ioasysapp/app/modules/home/presenters/home/home_states.dart';
import 'package:ioasysapp/app/shared/helpers/navigator_helper.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

@Injectable()
class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  IGetEnterprisesUsecase getEnterprisesUsecase;

  _HomeControllerBase(this.getEnterprisesUsecase) {
    setState(const StartHomeState());
  }

  @observable
  HomeStates state;

  @action
  HomeStates setState(HomeStates value) => state = value;

  Future<void> searchEnterprises(String name) async {
    setState(const LoadingHomeState());
    final result = await getEnterprisesUsecase.getEnterprisesByName(name);
    result.fold(
      (error) => {
        setState(const ErrorHomeState()),
      },
      (List<EnterpriseEntity> listEnterprises) => {
        setState(SuccessHomeState(listEnterprises)),
      },
    );
  }
}
