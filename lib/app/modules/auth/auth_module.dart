import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/core/domain/repositories/authentication_repository_interface.dart';
import 'package:ioasysapp/app/core/domain/usecases/authentication/authentication_usecase.dart';
import 'package:ioasysapp/app/core/domain/usecases/authentication/authentication_usecase_interface.dart';
import 'package:ioasysapp/app/core/external/datasources/authentication_datasource.dart';
import 'package:ioasysapp/app/core/infra/datasources/authenticate_datasource_interface.dart';
import 'package:ioasysapp/app/core/infra/repositories/authentication_repository.dart';
import 'package:ioasysapp/app/modules/auth/presenters/login/login_page.dart';

import 'presenters/login/login_controller.dart';

class AuthModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => LoginController(i.get<IAuthenticationUsecase>())),
        Bind((i) => AuthenticationDatasource(i.get<Dio>())),
        Bind((i) => AuthenticationRepository(i.get<IAuthenticationDatasource>())),
        Bind((i) => AuthenticationUsecase(i.get<IAuthenticationRepository>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => const LoginPage()),
      ];

  static Inject get to => Inject<AuthModule>.of();
}
