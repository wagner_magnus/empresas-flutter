import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/core/domain/entities/authenticate_entity.dart';
import 'package:ioasysapp/app/core/domain/entities/oauth_token_entity.dart';
import 'package:ioasysapp/app/core/domain/usecases/authentication/authentication_usecase_interface.dart';
import 'package:ioasysapp/app/shared/helpers/app_routes_helper.dart';
import 'package:ioasysapp/app/shared/helpers/loader_helper.dart';
import 'package:ioasysapp/app/shared/helpers/navigator_helper.dart';
import 'package:mobx/mobx.dart';
import 'package:validadores/ValidarEmail.dart';

part 'login_controller.g.dart';

@Injectable()
class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  IAuthenticationUsecase authenticationUsecase;

  _LoginControllerBase(this.authenticationUsecase);

  @observable
  bool visiblePassword = false;

  @observable
  String email;

  @observable
  String password;

  @observable
  bool errorLogin = false;

  @action
  void updateVisiblePassword() => visiblePassword = !visiblePassword;

  @action
  void setEmail(String value) {
    errorLogin = false;
    email = value;
  }

  @action
  void setPassword(String value) {
    errorLogin = false;
    password = value;
  }

  @action
  void removeErrorLogin() => errorLogin = false;

  @action
  void setErrorLogin() => errorLogin = true;

  @action
  Future<void> login() async {
    if (!validadeFields()) {
      errorLogin = true;
      return false;
    }
    LoaderHelper.of(Modular.navigatorKey.currentContext).showLoading();
    final credential = AuthenticateEntity(email: email, password: password);
    await _authenticate(credential);
  }

  Future<void> _authenticate(AuthenticateEntity authenticateEntity) async {
    final result = await authenticationUsecase.authenticate(authenticateEntity);
    return result.fold(
      (error) => {NavigatorHelper.pop(), setErrorLogin()},
      (OAuthTokenEntity oAuthTokenEntity) => {
        NavigatorHelper.pop(),
        NavigatorHelper.pushReplacementNamedTo(AppRoutesHelper.home),
      },
    );
  }

  bool validadeFields() {
    if (email == null || email.isEmpty || !EmailValidator.validate(email)) {
      return false;
    }

    if (password == null || password.isEmpty) {
      return false;
    }

    return true;
  }
}
