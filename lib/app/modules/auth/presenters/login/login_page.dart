import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/modules/auth/presenters/login/widgets/custom_button_widget.dart';
import 'package:ioasysapp/app/modules/auth/presenters/login/widgets/custom_text_field_widget.dart';
import 'package:ioasysapp/app/modules/auth/presenters/login/widgets/header_widget.dart';
import 'package:ioasysapp/theme/app_colors.dart';

import '../login/login_controller.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              HeaderWidget(),
              Observer(
                  builder: (_) => TextInputWidget(
                        onTapIconError: controller.removeErrorLogin,
                        hasError: controller.errorLogin,
                        label: 'Email',
                        onChange: controller.setEmail,
                      )),
              Observer(
                builder: (_) => TextInputWidget(
                  onTapIconError: controller.removeErrorLogin,
                  hasError: controller.errorLogin,
                  messageError: 'Credenciais incorretas',
                  label: 'Senha',
                  suffixIcon: IconButton(
                      icon: Icon(controller.visiblePassword ? Icons.visibility_off : Icons.visibility, color: AppColors.hintColor),
                      onPressed: controller.updateVisiblePassword),
                  obscureText: !controller.visiblePassword,
                  onChange: controller.setPassword,
                  showLabelError: controller.errorLogin,
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: CustomButtonWidget(title: 'Entrar', onPressed: controller.login)
              ),
            ],
          ),
        ),
      );
}
