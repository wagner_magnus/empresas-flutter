import 'package:flutter/material.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';
import 'package:ioasysapp/theme/text_styles.dart';

class HeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final paddingTop = MediaQuery.of(context).padding.top;
    return ClipPath(
      clipper: BottomOvalClipper(),
      child: Container(
        padding: EdgeInsets.only(bottom: 130, top: paddingTop + 70),
        width: double.maxFinite,
        decoration: const BoxDecoration(image: DecorationImage(fit: BoxFit.fill, image: AssetImage(AssetsPathHelper.background))),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 15),
              child: Image.asset(AssetsPathHelper.logo),
            ),
            const Text('Seja bem vindo ao empresas!', style: TextStyles.superTitle)
          ],
        ),
      ),
    );
  }
}

class BottomOvalClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path()..lineTo(0, size.height - 80);

    final firstControlPoint = Offset(size.width / 2, size.height);
    final firstEndPoint = Offset(size.width, size.height - 80);
    path
      ..quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, firstEndPoint.dx, firstEndPoint.dy)
      ..lineTo(size.width, 0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}
