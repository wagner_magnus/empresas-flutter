import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ioasysapp/theme/app_colors.dart';
import 'package:ioasysapp/theme/text_styles.dart';

class CustomButtonWidget extends StatelessWidget {
  final Color borderColor;
  final String title;
  final VoidCallback onPressed;
  final bool loading;
  final EdgeInsetsGeometry padding;
  final Widget widget;
  final bool disabled;

  const CustomButtonWidget({
    this.borderColor = AppColors.pink,
    this.title,
    this.onPressed,
    this.loading,
    this.padding,
    this.widget,
    this.disabled = false,
  }) : super();

  @override
  Widget build(BuildContext context) => MaterialButton(
      onPressed: disabled ? null : onPressed,
      padding: padding,
      height: 48,
      color: AppColors.pink,
      textColor: AppColors.white,
      disabledColor: AppColors.gray,
      disabledTextColor: AppColors.mono,
      elevation: 0,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.horizontal(
        left: Radius.circular(8),
        right: Radius.circular(8),
      )),
      minWidth: double.infinity,
      child: widget ?? Text(title, style: TextStyles.title, textAlign: TextAlign.center));

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ColorProperty('borderColor', borderColor))
      ..add(StringProperty('title', title))
      ..add(ObjectFlagProperty.has('onPressed', onPressed))
      ..add(DiagnosticsProperty<bool>('loading', loading))
      ..add(DiagnosticsProperty<EdgeInsetsGeometry>('padding', padding))
      ..add(DiagnosticsProperty<bool>('disabled', disabled));
  }
}
