import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:ioasysapp/app/shared/helpers/assets_path_helper.dart';
import 'package:ioasysapp/theme/app_colors.dart';
import 'package:ioasysapp/app/shared/helpers/fonts_helper.dart';

class TextInputWidget extends StatelessWidget {
  final String label;
  final Function(String) onChange;
  final Function onTapIconError;
  final bool obscureText;
  final bool hasError;
  final String messageError;
  final Widget suffixIcon;
  final bool showLabelError;

  const TextInputWidget(
      {@required this.label,
      Key key,
      this.onChange,
      this.obscureText = false,
      this.suffixIcon,
      this.hasError = false,
      this.showLabelError = false,
      this.onTapIconError,
      this.messageError})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Text(label, style: TextStyle(fontSize: 14, color: AppColors.hintColor, fontFamily: FontsHelper.regular)),
            ),
            Container(
              decoration:
                  BoxDecoration(border: Border.all(color: hasError ? Colors.red : Colors.transparent), borderRadius: BorderRadius.circular(4)),
              child: TextFormField(
                onChanged: onChange,
                obscureText: obscureText,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(12),
                  fillColor: AppColors.gray,
                  filled: true,
                  suffixIcon: hasError ? GestureDetector(onTap: () => onTapIconError, child: Image.asset(AssetsPathHelper.error), ) : suffixIcon,
                  border: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(4)),
                ),
              ),
            ),
            Visibility(
                visible: showLabelError,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 4),
                  margin: const EdgeInsets.only(top: 4),
                  width: double.maxFinite,
                  child: Text(
                    messageError ?? '',
                    textAlign: TextAlign.end,
                    style: const TextStyle(color: AppColors.red, fontSize: 12, fontFamily: FontsHelper.light),
                  ),
                ))
          ],
        ),
      );
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(StringProperty('label', label))
      ..add(DiagnosticsProperty<Function(String p1)>('onChange', onChange))
      ..add(DiagnosticsProperty<Function>('onTapIconError', onTapIconError))
      ..add(DiagnosticsProperty<bool>('obscureText', obscureText))
      ..add(DiagnosticsProperty<bool>('hasError', hasError))
      ..add(DiagnosticsProperty<bool>('showLabelError', showLabelError))
      ..add(StringProperty('messageError', messageError));
  }
}
