import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:ioasysapp/app/app_widget.dart';
import 'package:ioasysapp/app/modules/auth/auth_module.dart';
import 'package:ioasysapp/app/modules/home/home_module.dart';
import 'package:ioasysapp/app/modules/splashscreen/splashscreen_module.dart';
import 'package:ioasysapp/app/shared/dio/base_dio.dart';
import 'package:ioasysapp/app/shared/helpers/app_routes_helper.dart';

import 'app_controller.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => BaseDio(Dio()).createDio()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: SplashscreenModule()),
        ModularRouter(AppRoutesHelper.login, module: AuthModule()),
        ModularRouter(AppRoutesHelper.home, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
