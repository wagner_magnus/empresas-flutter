import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ioasysapp/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
    // Modo retrato obrigatorio
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarBrightness: Brightness.dark),
    );
  
  await DotEnv().load('.env');

  runApp(ModularApp(module: AppModule()));
}
